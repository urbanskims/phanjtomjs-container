# Install PhantomJS
set -e
apt-get -y install libfreetype6 libfreetype6-dev fontconfig libicu52 libjpeg8 libfontconfig libwebp5 unzip

ARCH=`uname -m`
PHANTOMJS_VERSION=2.0.0-20150528
PHANTOMJS_URL=https://github.com/bprodoehl/phantomjs/releases/download/v2.0.0-20150528/phantomjs-2.0.0-20150528-u1404-x86_64.zip
PHANTOMJS_ZIP_FILE=phantomjs-2.0.0-20150528-u1404-x86_64.zip

cd /usr/local/share/
curl -L -O $PHANTOMJS_URL
unzip $PHANTOMJS_ZIP_FILE
ln -s -f /usr/local/share/phantomjs-${PHANTOMJS_VERSION}/bin/phantomjs /usr/local/share/phantomjs
ln -s -f /usr/local/share/phantomjs-${PHANTOMJS_VERSION}/bin/phantomjs /usr/local/bin/phantomjs
ln -s -f /usr/local/share/phantomjs-${PHANTOMJS_VERSION}/bin/phantomjs /usr/bin/phantomjs

rm $PHANTOMJS_ZIP_FILE
