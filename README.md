## Fundwerks Docker Images 

This repository contains the requsite images necessary to run phantomJS tests using chimp and PhantomJS.


### Requirements

* docker

### Running the tests

The following commands will build the container and check that phantomjs is installed and running properly.
~~~shell
cd tests
./run_tests
~~~


### Building the container

To build the container run the following
~~~shell
cd testcontainer
./build.sh
~~~
